# ProxySQL datadog

ProxySQL datadog permissions implementation.

Run the role with something like this:
```
  - name: "Add datadog permissions"
    hosts: all
    become: True
    roles:
      - proxysql-datadog
```

Configure datadog with something like this:
```
datadog_enabled: yes
datadog_config:
  process_config:
    enabled: "false"
  tags:
    - "env:{{ dtap_environment }}"
    - "environment:{{ dtap_environment }}"
  logs_enabled: true
datadog_checks:
  process:
    init_config:
    instances:
      - name: proxysql
        search_string: ['proxysql']
        exact_match: false
    logs:
      - type: file
        path: /var/lib/proxysql/proxysql.log
        service: proxysql
        source: proxysql
```